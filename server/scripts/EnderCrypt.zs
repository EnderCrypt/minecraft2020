// divining rods
print("Removing divining rods");
recipes.remove(<projecte:item.pe_divining_rod_1>);
recipes.remove(<projecte:item.pe_divining_rod_2>);
recipes.remove(<projecte:item.pe_divining_rod_3>);

// gem armour
print("Removing gem armours");
recipes.remove(<projecte:item.pe_gem_armor_0>);
recipes.remove(<projecte:item.pe_gem_armor_1>);
recipes.remove(<projecte:item.pe_gem_armor_2>);
recipes.remove(<projecte:item.pe_gem_armor_3>);

// repair talisman
print("Removing repair talisman");
recipes.remove(<projecte:item.pe_repair_talisman>);

// arcana ring
print("Removing arcana ring");
recipes.remove(<projecte:item.pe_arcana_ring>);

// collector
print("Making energy collectors more expensive");
recipes.removeShaped(<projecte:collector_mk1>);
recipes.addShaped("projecte_collector_mk1", <projecte:collector_mk1>, [[<ore:glowstone>, <ore:blockDiamond>, <ore:glowstone>], [<ore:glowstone>, <ore:blockDiamond>, <ore:glowstone>], [<ore:glowstone>, <minecraft:furnace>, <ore:glowstone>]]);

recipes.removeShaped(<projecte:collector_mk2>);
recipes.addShaped("projecte_collector_mk2", <projecte:collector_mk2>, [[<ore:glowstone>, <projecte:item.pe_matter>, <ore:glowstone>], [<ore:glowstone>, <projecte:collector_mk1>, <ore:glowstone>], [<ore:glowstone>, <projecte:item.pe_matter>, <ore:glowstone>]]);

recipes.removeShaped(<projecte:collector_mk3>);
recipes.addShaped("projecte_collector_mk3", <projecte:collector_mk3>, [[<ore:glowstone>, <projecte:item.pe_matter:1>, <ore:glowstone>], [<ore:glowstone>, <projecte:collector_mk2>, <ore:glowstone>], [<ore:glowstone>, <projecte:item.pe_matter:1>, <ore:glowstone>]]);

// slimesling
recipes.remove(<tconstruct:slimesling>);
recipes.remove(<tconstruct:slimesling:1>);
recipes.remove(<tconstruct:slimesling:2>);
recipes.remove(<tconstruct:slimesling:3>);
recipes.remove(<tconstruct:slimesling:4>);
recipes.remove(<tconstruct:slimesling:5>);

// slime boots
recipes.remove(<tconstruct:slime_boots>);
recipes.remove(<tconstruct:slime_boots:1>);
recipes.remove(<tconstruct:slime_boots:2>);
recipes.remove(<tconstruct:slime_boots:3>);
recipes.remove(<tconstruct:slime_boots:4>);
recipes.remove(<tconstruct:slime_boots:5>);
