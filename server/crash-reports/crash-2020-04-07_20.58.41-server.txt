---- Minecraft Crash Report ----

WARNING: coremods are present:
  TransformerLoader (OpenComputers-MC1.12.2-1.7.5.192.jar)
  OTGCorePlugin (OTG-Core.jar)
  MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.244.jar)
  CTMCorePlugin (CTM-MC1.12.2-1.0.2.31.jar)
Contact their authors BEFORE contacting forge

// Why is it breaking :(

Time: 4/7/20 8:58 PM
Description: Exception in server tick loop

java.lang.RuntimeException: Tried to register biome openterraingenerator:overworld_frozen_forest to a id 43 but it is occupied by biome: galacticraftplanets:outer space 2. This can happen when using the CustomBiomes setting in the world config or when changing mod/biome configurations for previously created worlds. OTG 1.12.2 v7 and above use dynamic biome id's for new worlds, this avoids the problem completely.
	at com.pg85.otg.forge.biomes.ForgeBiomeRegistryManager.registerForgeBiomeWithId(ForgeBiomeRegistryManager.java:436)
	at com.pg85.otg.forge.biomes.ForgeBiomeRegistryManager.getOrCreateBiome(ForgeBiomeRegistryManager.java:126)
	at com.pg85.otg.forge.ForgeWorld.createBiomeFor(ForgeWorld.java:429)
	at com.pg85.otg.network.ServerConfigProvider.createAndRegisterBiome(ServerConfigProvider.java:632)
	at com.pg85.otg.network.ServerConfigProvider.indexSettings(ServerConfigProvider.java:507)
	at com.pg85.otg.network.ServerConfigProvider.loadBiomes(ServerConfigProvider.java:208)
	at com.pg85.otg.network.ServerConfigProvider.loadSettings(ServerConfigProvider.java:134)
	at com.pg85.otg.network.ServerConfigProvider.<init>(ServerConfigProvider.java:74)
	at com.pg85.otg.forge.world.WorldLoader.getOrCreateForgeWorld(WorldLoader.java:362)
	at com.pg85.otg.forge.world.OTGWorldType.getBiomeProvider(OTGWorldType.java:146)
	at net.minecraft.world.WorldProvider.func_76572_b(WorldProvider.java:58)
	at net.minecraft.world.WorldProvider.func_76558_a(WorldProvider.java:40)
	at net.minecraft.world.WorldServer.<init>(WorldServer.java:116)
	at net.minecraft.server.MinecraftServer.func_71247_a(MinecraftServer.java:298)
	at net.minecraft.server.dedicated.DedicatedServer.func_71197_b(DedicatedServer.java:270)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:486)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Linux (amd64) version 5.3.0-7642-generic
	Java Version: 1.8.0_242, Private Build
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode), Private Build
	Memory: 1027417136 bytes (979 MB) / 1598029824 bytes (1524 MB) up to 2330460160 bytes (2222 MB)
	JVM Flags: 2 total; -Xms1024m -Xmx2500m
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2847 24 mods loaded, 24 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State  | ID                   | Version             | Source                                    | Signature |
	|:------ |:-------------------- |:------------------- |:----------------------------------------- |:--------- |
	| LCHIJA | minecraft            | 1.12.2              | minecraft.jar                             | None      |
	| LCHIJA | mcp                  | 9.42                | minecraft.jar                             | None      |
	| LCHIJA | FML                  | 8.0.99.99           | forge-1.12.2-14.23.5.2847-universal.jar   | None      |
	| LCHIJA | forge                | 14.23.5.2847        | forge-1.12.2-14.23.5.2847-universal.jar   | None      |
	| LCHIJA | micdoodlecore        |                     | minecraft.jar                             | None      |
	| LCHIJA | otgcore              | 1.12.2 - v8.2       | minecraft.jar                             | None      |
	| LCHIJA | opencomputers|core   | 1.7.5.192           | minecraft.jar                             | None      |
	| LCHIJA | battletowers         | 1.6.5               | BattleTowers-1.12.2.jar                   | None      |
	| LCHIJA | baubles              | 1.5.2               | Baubles-1.12-1.5.2.jar                    | None      |
	| LCHIJA | openterraingenerator | v8.2                | OpenTerrainGenerator-1.12.2+-+v8.2.jar    | None      |
	| LCHIJA | biomebundle          | 5.1                 | Biome_Bundle-1.12.2-v6.1.jar              | None      |
	| LCHIJA | chisel               | MC1.12.2-1.0.2.45   | Chisel-MC1.12.2-1.0.2.45.jar              | None      |
	| LCHIJA | crafttweaker         | 4.1.20              | CraftTweaker2-1.12-4.1.20.jar             | None      |
	| LCHIJA | crafttweakerjei      | 2.0.3               | CraftTweaker2-1.12-4.1.20.jar             | None      |
	| LCHIJA | mantle               | 1.12-1.3.3.55       | Mantle-1.12-1.3.3.55.jar                  | None      |
	| LCHIJA | tconstruct           | 1.12.2-2.13.0.183   | TConstruct-1.12.2-2.13.0.183.jar          | None      |
	| LCHIJA | galacticraftcore     | 4.0.2.244           | GalacticraftCore-1.12.2-4.0.2.244.jar     | None      |
	| LCHIJA | galacticraftplanets  | 4.0.2.244           | Galacticraft-Planets-1.12.2-4.0.2.244.jar | None      |
	| LCHIJA | improvedbackpacks    | 1.12.2-1.4.0.0      | ImprovedBackpacks-1.12.2-1.4.0.0.jar      | None      |
	| LCHIJA | thaumcraft           | 6.1.BETA26          | Thaumcraft-1.12.2-6.1.BETA26.jar          | None      |
	| LCHIJA | lycanitesmobs        | 2.0.0.4 - MC 1.12.2 | lycanitesmobs-1.12.2-2.0.0.4.jar          | None      |
	| LCHIJA | opencomputers        | 1.7.5.192           | OpenComputers-MC1.12.2-1.7.5.192.jar      | None      |
	| LCHIJA | projecte             | 1.12.2-PE1.4.1      | ProjectE-1.12.2-PE1.4.1.jar               | None      |
	| LCHIJA | waystones            | 4.1.0               | Waystones_1.12.2-4.1.0.jar                | None      |

	Loaded coremods (and transformers): 
TransformerLoader (OpenComputers-MC1.12.2-1.7.5.192.jar)
  li.cil.oc.common.asm.ClassTransformer
OTGCorePlugin (OTG-Core.jar)
  com.pg85.otg.forge.asm.excluded.OTGClassTransformer
MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.244.jar)
  micdoodle8.mods.miccore.MicdoodleTransformer
CTMCorePlugin (CTM-MC1.12.2-1.0.2.31.jar)
  team.chisel.ctm.client.asm.CTMTransformer
	Pulsar/tconstruct loaded Pulses: 
		- TinkerCommons (Enabled/Forced)
		- TinkerWorld (Enabled/Not Forced)
		- TinkerTools (Enabled/Not Forced)
		- TinkerHarvestTools (Enabled/Forced)
		- TinkerMeleeWeapons (Enabled/Forced)
		- TinkerRangedWeapons (Enabled/Forced)
		- TinkerModifiers (Enabled/Forced)
		- TinkerSmeltery (Enabled/Not Forced)
		- TinkerGadgets (Enabled/Not Forced)
		- TinkerOredict (Enabled/Forced)
		- TinkerIntegration (Enabled/Forced)
		- TinkerFluids (Enabled/Forced)
		- TinkerMaterials (Enabled/Forced)
		- TinkerModelRegister (Enabled/Forced)
		- chiselIntegration (Enabled/Not Forced)

	Profiler Position: N/A (disabled)
	Player Count: 0 / 20; []
	Is Modded: Definitely; Server brand changed to 'fml,forge'
	Type: Dedicated Server (map_server.txt)