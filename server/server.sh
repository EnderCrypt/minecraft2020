#!/bin/bash

cd $(dirname $0)

java -Xms1024m -Xmx2500m -jar forge-1.12.2-14.23.5.2847-universal.jar nogui

exit $?
