local filesystem = require("filesystem")
local internet = require("internet")
local shell = require("shell")
local component = require("component")

-- variables
local serviceUrl = "http://endercrypt.network:4545/youtubetape"
local serviceKey = "SupahSecret"

-- arguments
local args, options = shell.parse(...)
local text = args[1]
local index1, index2 = string.find(text, "?v=")
local text = string.sub(text, index2+1)
local index1, index2 = string.find(text, "&list=")
if (index1 ~= nil) then
  text = string.sub(text, 1, index1 - 1)
end
local videoID = text

-- library
function iteratorCollect(iterator)
  local response = ""
  for chunk in iterator do
    response = response..chunk
  end
  return response
end

function round(value, decimals)
  local mult = math.pow(10, decimals)
  return math.floor((value * mult) + 0.5) / mult
end

-- internet
function internetRequest(url, data, headers)
  return internet.request(url, data, headers)
end

function request(operation)
  return internetRequest(serviceUrl.."/"..operation.."?video="..videoID, {}, {key=serviceKey})
end

-- tape
if not component.isAvailable("tape_drive") then
  io.stderr:write("This program requires a tape drive to run.")
  return
end

local function getTapeDrive()
  --Credits to gamax92 for this
  local tape
  if options.address then
    if type(options.address) ~= "string" then
      io.stderr:write("'address' may only be a string.")
      return
    end
    local fulladdr = component.get(options.address)
    if fulladdr == nil then
      io.stderr:write("No component at this address.")
      return
    end
    if component.type(fulladdr) ~= "tape_drive" then
      io.stderr:write("No tape drive at this address.")
      return
    end
    tape = component.proxy(fulladdr)
  else
    tape = component.tape_drive
  end
  return tape
  --End of gamax92's part
end

local tape = getTapeDrive()

function tape_reset()
  tape.stop()
  tape.seek(-tape.getSize())
  tape.stop() --Just making sure
end

-- create
print("requesting youtube video "..videoID.."...")
request("create")

-- loop status check
print("waiting for response...")
while (true) do
  os.sleep(0.25)
  local response = iteratorCollect(request("status"))
  if (response == "ERROR") then
    error("an internal server error occured")
    return
  end
  if (response == "DONE") then
    break
  end
  print("status: "..response)
end

-- download & writting
print("downloading and writting to tape...")
tape_reset()
local tape_size = tape.getSize()
print("tape size: "..tape_size.." bytes")
local c = 0
local b = 0
for chunk in request("download") do
  c = c + 1
  local size = string.len(chunk)
  local overflow = (b+size)-tape_size
  if (overflow > 0) then
    chunk = string.sub(chunk, 1, string.len(chunk)-overflow)
    size = string.len(chunk)
  end
  b = b + size
  tape.write(chunk)
  print("wrote chunk "..c.." ("..b.." bytes) tape filled to "..round(100/tape_size*b, 1).."% of capacity")
  if (b >= tape_size) then
    print("tape filled!")
  end
end
tape_reset()

-- done
print("done!")
