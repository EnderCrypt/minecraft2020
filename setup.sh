#!/bin/bash

set -e

function link_directory {
  target_dir="$1/mods/"
  source="mods/$2/*"

  for path in $source
  do
    if test -f "$path"; then
      echo "Mod: $(basename $path)"
      ln -s ../../$path $target_dir
    fi
    if test -d "$path"; then
      echo "Mod folder: $(basename $path)"
      ln -s ../../$path $target_dir
    fi
  done
}

function link_mods {
  target="$1"
  echo "setting up $target mods ..."

  rm -rf $target/mods/*
  link_directory $target "${target}_only"
  link_directory $target "global"
}

function zip_client_mods {
  echo "Zipping client mods ..."
  target_zip="everything"
  cd client/
  rm -f ${target_zip}.zip
  zip -r ${target_zip}.zip *
  cd ../
}

cd $(dirname $0)

link_mods "client"
link_mods "server"

zip_client_mods

pwd

echo "done!"
